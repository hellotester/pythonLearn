s = input('请输入一个字符串:\n')
print(s)
letters = 0
space = 0
digits = 0
others = 0

for i in s:
    if i.isalpha():
        letters += 1
    elif i.isdigit():
        digits += 1
    elif i.isspace():
        space += 1
    else:
        others += 1
print('letters=%d,space=%d,digits=%d,others=%d' % (letters, space, digits, others))
