
#第一种方法 使用sorted排序

def use_sort(l):
    return sorted(l)

# 第二种方法 交换排序（冒泡排序）


# 第二种方法 冒泡排序

def bubble_sort(l):
    """
    它重复地走访过要排序的数列，一次比较两个元素，如果他们的顺序错误就把他们交换过来。
    走访数列的工作是重复地进行直到没有再需要交换，也就是说该数列已经排序完成
    """
    for i in range(len(l)):
        for j in range(i, len(s)):
            if l[j] <= l[i]:
                l[i], l[j] = l[j], l[i]
    return l

if __name__=='__main__':
    s = input("请输入数字：\n")
    # s='1253311'
    l = []
    for i in s:
        l.append(i)

    print(use_sort(l))
    print(bubble_sort(l))

