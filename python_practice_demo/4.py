#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/11/16 11:09
# @Author  : LuChao
# @Site    :
# @File    : 4.py
# @Software: PyCharm

import turtle

turtle.width(10)
turtle.color("blue")
turtle.circle(50)

turtle.penup()
turtle.goto(120,0)
turtle.color("black")
turtle.pendown()
turtle.circle(50)

turtle.penup()
turtle.goto(60,0)
turtle.color("red")
turtle.pendown()
turtle.circle(50)

