#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/9/11 11:21
# @Author  : LuChao
# @Site    : 
# @File    : GetContent.py
# @Software: PyCharm
import re


def get_result(rec, str):
    """
    :param rec: 匹配规则
    :param str: 匹配的对象
    :return: 查询的结果
    """
    result = re.findall(rec, str)
    return result

def get_index_names(str):
    """
    提出索引名称
    :param str:
    :return:
    """
    index_rc1 = re.compile(r'CREATE INDEX [\S]*?(\w+)[^\s] ON', flags=re.I)
    index_rc2 = re.compile(r'CREATE UNIQUE INDEX[\S]*?(\w+)[^\s] ON', flags=re.I)
    result_index_name = get_result(index_rc1, str) + get_result(index_rc2, str)
    return result_index_name


def get_table_names(str):
    """
    提取表名称
    :param str:
    :return:
    """
    table_name_rc1 = re.compile(r'CREATE TABLE \S*?(\w+).*\(', flags=re.I)
    table_name_rc2 = re.compile(r'CREATE TABLE  if not exists [\S]*?(\w+)[\S].*?\(', flags=re.I)
    result_table_name = get_result(table_name_rc1, str) + get_result(table_name_rc2, str)
    return result_table_name


def get_colu_names(str):
    """
    提取列名称
    :param str:
    :return:
    """

    colu_name_rc1 = re.compile(r"`(\w+)`\s[vbcitd]", flags=re.I)
    colu_name_rc2 = re.compile(r'add column (\w+)\s[vbcitd]', flags=re.I)
    result_colu_name = get_result(colu_name_rc1, str) + get_result(colu_name_rc2, str)
    return result_colu_name


def get_produce_names(str):
    """
    提取存储过程名称
    :param str:
    :return:
    """

    produce_name_rc = re.compile('CREATE.*PROCEDURE (\w+).*\(', flags=re.I)
    print(str)
    result_produce_name = get_result(produce_name_rc, str)
    return result_produce_name


def get_view_names(str):
    """
    提取视图
    :param str:
    :return:
    """
    view_name_rc = re.compile('CREATE VIEW [\S]*?(\w+)[^\s]\(', flags=re.I)
    result_view_name = get_result(view_name_rc, str)
    return result_view_name


def get_func_names(str):
    """
    提取方法
    :param str:
    :return:
    """
    func_name_rc = re.compile('CREATE FUNCTION[\S]*?(\w+)[^\s]\(', flags=re.I)
    result_fuc_name = get_result(func_name_rc, str)
    return result_fuc_name


def is_match_t(strs):

    """
    表名称规则校验：t_开头小写字母和下划线
    :param strs:
    :return: 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'
    for i in strs:
        if re.match(r'^t_[a-z_]+$', i) is None:
            return 0, '创建表名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg


def is_match_colu(strs):

    """
    列名称规则校验：小写字母和下划线组成
    :param strs:
    :return: code 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'
    for i in strs:
        if re.match(r'[a-z_]+$', i.strip()) is None:
            return 0, '创建列名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg


def is_match_p(strs):
    """
    存储过程校验：p_开头
    :param strs:
    :return: code 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'
    for i in strs:
        if re.match(r'^p_(.*?)', i) is None:
            return 0, '存储过程名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg


def is_match_f(strs):
    """
    函数校验：f_开头
    :param strs:
    :return: code 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'

    for i in strs:
        if re.match(r'^f_(.*?)', i) is None:
            return 0, '方法名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg

def is_match_v(strs):
    """
    视图校验：f_开头
    :param strs:
    :return: code 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'
    for i in strs:
        if re.match(r'^v_(.*?)', i) is None:
            return 0, '视图名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg


def is_match_indx(strs):

    """
    索引校验：idx_开头
    :param strs:
    :return: code 1通过，0不通过，msg返回信息
    """

    code, msg = 1, '验证通过'

    for i in strs:
        if re.match(r'^idx_(.*?)', i) is None:
            return 0, '索引名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg


def run(str):
    index_names = get_index_names(str)
    table_names = get_table_names(str)
    produce_names = get_produce_names(str)
    view_names = get_view_names(str)
    func_names = get_func_names(str)
    colu_names = get_colu_names(str)

    print(table_names)


    code, msg = 1, ''
    if index_names and code:
        code, msg = is_match_indx(index_names)
    if table_names and code:
        code, msg = is_match_t(table_names)
    if produce_names and code:
        code, msg = is_match_p(produce_names)
    if view_names and code:
        code, msg = is_match_v(view_names)
    if func_names and code:
        code, msg = is_match_f(func_names)
    if colu_names and code:
        code, msg = is_match_colu(colu_names)
    return code, msg


if __name__ == '__main__':

    path = '1.text'
    with open(path,encoding='UTF-8') as op:
        str=op.read()
    print(run(str))



