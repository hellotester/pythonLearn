## python校验SQL脚本命名规则



#### 需求背景
检查脚本文件中SQL语句是否按规范编写，规则如下：
- 创建表时，表名称需以"t_"开头且均为小写
- 增加和创建列时，列名称均为小写字母和_组成
- 创建函数，函数名称需以"f_"开头
- 创建存储过程，存储过程名称需以"p_"开头
- 创建索引，索引名称需以"idx_"开头
- 创建视图，视图名称需以"v_"开头
#### 需求分析
1. 首先要从脚本文件中提取出来表名称、列名称、函数名称、存储过程名称、索引名称、视图名称

   这里需要使用python 相应的re和os模块

2. 分别创建相应的规则，相应的名称依次分别进行校验，返回信息和具体提示



### 代码实现

以校验表名称为例
文本内容如下；

```db2
-- mysql创建view、trigger、function、procedure、event时都会定义一个Definer
-- 更新中！！！！！！！！！！！！！
CREATE TABLE `t_auth_group_permissions`  (
create table t_business_apply_reback(
CREATE TABLE `t_exception_record` (
CREATE TABLE `t_lend_channel_insurance_company` (

alter table hb_product.admit_params add column originalid varchar(80);
alter table hb_product.admit_params_detail add column originalid varchar(80);
```

1. 第一步——提取表名称

```python

def get_result(rec, str):
    """
    :param rec: 匹配规则
    :param str: 匹配的对象
    :return: 查询的结果
    """
    result = re.findall(rec, str)
    return result

def get_table_names(str):
    """
    提取表名称返回数组
    :param str:
    :return:
    """
    table_name_rc1 = re.compile(r'CREATE TABLE [\S]*?(\w+)[\S].*\(', flags=re.I)
    result_table_name = get_result(table_name_rc1, str) + get_result(table_name_rc2, str)
    return result_table_name
    
```
* flags=re.I 匹配时忽略大小写，因为我们的表的创建语句CREATE TABLE或create table t_business_apply_reback
* \w 匹配字母数字下划线
* \S 匹配任意空字符
* [] 用来表示一组字符
* () 匹配括号内的表达式
* \* 匹配0个或多个
* .  匹配任意字符除了换行符
* ？ 匹配0个或多个，非贪婪模式

运行效果如下：
>['t_auth_group_permissions', 't_business_apply_rebac', 'S_exception_record', 't_lend_channel_insurance_company']

2. 第二步——表名称匹配

```python

def is_match_t(strs):

    """
    表名称规则校验：t_开头小写字母和下划线
    :param strs:
    :return: 1通过，0不通过，msg返回信息
    """
    code, msg = 1, '验证通过'
    for i in strs:
        if re.match(r'^t_[a-z_]+$', i) is None:
            return 0, '创建表名称:' + i + '不符合命名规范，请仔细检查！'
    return code, msg
```
* ^ 匹配字符串的开头
* $ 匹配字符串的结尾
* [a-z_] 匹配小写字母和下划线

运行效果如下：
>(0, '创建表名称:S_exception_record不符合命名规范，请仔细检查！')
